
msgid ""
msgstr ""
"Project-Id-Version: developers-reference \n"
"Report-Msgid-Bugs-To: \n"
"POT-Creation-Date: 2019-10-05 13:33+0000\n"
"PO-Revision-Date: 2020-03-20 12:36+0900\n"
"Last-Translator: FULL NAME <EMAIL@ADDRESS>\n"
"Language-Team: LANGUAGE <LL@li.org>\n"
"MIME-Version: 1.0\n"
"Content-Type: text/plain; charset=utf-8\n"
"Content-Transfer-Encoding: 8bit\n"
"Generated-By: Babel 2.6.0\n"

#: ../developer-duties.rst:4
msgid "Debian Developer's Duties"
msgstr "Debian 開発者の責務"

#: ../developer-duties.rst:9
msgid "Package Maintainer's Duties"
msgstr "パッケージメンテナの責務"

#: ../developer-duties.rst:11
msgid ""
"As a package maintainer, you're supposed to provide high-quality packages"
" that are well integrated into the system and that adhere to the Debian "
"Policy."
msgstr ""

#: ../developer-duties.rst:18
msgid "Work towards the next ``stable`` release"
msgstr "次期安定版 (``stable``) リリースへの作業"

#: ../developer-duties.rst:20
msgid ""
"Providing high-quality packages in ``unstable`` is not enough; most users"
" will only benefit from your packages when they are released as part of "
"the next ``stable`` release. You are thus expected to collaborate with "
"the release team to ensure your packages get included."
msgstr ""

#: ../developer-duties.rst:25
msgid ""
"More concretely, you should monitor whether your packages are migrating "
"to ``testing`` (see :ref:`testing`). When the migration doesn't happen "
"after the test period, you should analyze why and work towards fixing "
"this. It might mean fixing your package (in the case of release-critical "
"bugs or failures to build on some architecture) but it can also mean "
"updating (or fixing, or removing from ``testing``) other packages to help"
" complete a transition in which your package is entangled due to its "
"dependencies. The release team might provide you some input on the "
"current blockers of a given transition if you are not able to identify "
"them."
msgstr ""
"より具体的には、パッケージがテスト版 (``testing``) に移行しているかどうかを見守る必要があります (:ref:`testing` "
"参照) "
"。テスト期間後に移行が行われない場合は、その理由を分析してこれを修正する必要があります。(リリースクリティカルバグや、いくつかのアーキテクチャでビルドに失敗する場合)"
" "
"あなたのパッケージを修正するのが必要な場合もありますし、依存関係でパッケージが絡まっている状態からの移行を完了する手助けとして、他のパッケージを更新"
" (あるいは修正、またはテスト版 (``testing``) からの削除) が必要な事を意味する場合もあります。障害となる理由 (blocker)"
" を判別できない場合は、リリースチームが先の移行に関する現在の障害に関する情報を与えてくれることでしょう。"

#: ../developer-duties.rst:39
msgid "Maintain packages in ``stable``"
msgstr "安定版 (``stable``) にあるパッケージをメンテナンスする"

#: ../developer-duties.rst:41
msgid ""
"Most of the package maintainer's work goes into providing updated "
"versions of packages in ``unstable``, but their job also entails taking "
"care of the packages in the current ``stable`` release."
msgstr ""
"パッケージメンテナの作業の大半は、パッケージの更新されたバージョンを不安定版 (``unstable``) へ放り込むことですが、現状の安定版 "
"(``stable``) リリースのパッケージの面倒をみることも伴っています。"

#: ../developer-duties.rst:45
msgid ""
"While changes in ``stable`` are discouraged, they are possible. Whenever "
"a security problem is reported, you should collaborate with the security "
"team to provide a fixed version (see :ref:`bug-security`). When bugs of "
"severity important (or more) are reported against the ``stable`` version "
"of your packages, you should consider providing a targeted fix. You can "
"ask the ``stable`` release team whether they would accept such an update "
"and then prepare a ``stable`` upload (see :ref:`upload-stable`)."
msgstr ""
"安定版 (``stable``) "
"への変更は推奨されてはいませんが、可能です。セキュリティ問題が報告された時はいつでも、セキュリティチームと修正版を提供するように協力する必要があります"
" (:ref:`bug-security` 参照)。important (あるいはそれ以上) な重要度のバグが安定版 (``stable``) "
"のバージョンのパッケージに報告されたら、対象となる修正の提供を検討する必要があります。安定版 (``stable``) "
"リリースマネージャに、そのような更新を受け入れられるかどうかを尋ね、それから安定版 (``stable``) "
"のアップロードを準備するなどができます (:ref:`upload-stable` 参照)。"

#: ../developer-duties.rst:57
msgid "Manage release-critical bugs"
msgstr "リリースクリティカルバグに対処する"

#: ../developer-duties.rst:59
msgid ""
"Generally you should deal with bug reports on your packages as described "
"in :ref:`bug-handling`. However, there's a special category of bugs that "
"you need to take care of — the so-called release-critical bugs (RC bugs)."
" All bug reports that have severity ``critical``, ``grave`` or "
"``serious`` make the package unsuitable for inclusion in the next "
"``stable`` release. They can thus delay the Debian release (when they "
"affect a package in ``testing``) or block migrations to ``testing`` (when"
" they only affect the package in ``unstable``). In the worst scenario, "
"they will lead to the package's removal. That's why these bugs need to be"
" corrected as quickly as possible."
msgstr ""
"大抵の場合、パッケージに対するバグ報告については :ref:`bug-handling`\\ "
"で記述されているように対応する必要があります。しかしながら、注意を必要とする特別なカテゴリのバグがあります—リリースクリティカルバグ (RC "
"bug) と呼ばれるものです。\\ ``critical``\\ 、\\ ``grave``\\ 、\\ ``serious`` "
"の重要度が付けられている全てのバグ報告によって、そのパッケージは次の安定版 (``stable``) "
"リリースに含めるのには適切ではないとされます。そのため、(テスト版 (``testing``) にあるパッケージに影響する場合に) Debian "
"のリリースを遅らせたり、(不安定版 (``unstable``) にあるパッケージにのみ影響する場合に) テスト版 (``testing``) "
"への移行をブロックする可能性があります。最悪の場合は、パッケージの削除を招きます。これが RC バグを可能な限り素早く修正する必要がある理由です。"

#: ../developer-duties.rst:70
msgid ""
"If, for any reason, you aren't able fix an RC bug in a package of yours "
"within 2 weeks (for example due to time constraints, or because it's "
"difficult to fix), you should mention it clearly in the bug report and "
"you should tag the bug ``help`` to invite other volunteers to chime in. "
"Be aware that RC bugs are frequently the targets of Non-Maintainer "
"Uploads (see :ref:`nmu`) because they can block the ``testing`` migration"
" of many packages."
msgstr ""
"もし、何らかの理由で 2 週間以内に RC バグを修正できない場合 "
"(例えば時間の制約上、あるいは修正が難しいなど)、明示的にバグ報告にそれを述べて、他のボランティアを招き入れて参加してもらうためにバグに "
"``help`` タグを打ってください。大量のパッケージが\\ ``テスト版 (testing)`` へ移行するのを妨げることがあるので、RC "
"バグは頻繁に Non-Maintainer Upload の対象になることに注意してください (:ref:`nmu` 参照)。"

#: ../developer-duties.rst:78
msgid ""
"Lack of attention to RC bugs is often interpreted by the QA team as a "
"sign that the maintainer has disappeared without properly orphaning their"
" package. The MIA team might also get involved, which could result in "
"your packages being orphaned (see :ref:`mia-qa`)."
msgstr ""
"RC バグへの関心の無さは、しばしば QA チームによって、メンテナが正しくパッケージを放棄せずに消えてしまったサインとして判断されます。MIA "
"チームが関わることもあり、その場合はパッケージが放棄されます (:ref:`mia-qa` 参照)。"

#: ../developer-duties.rst:86
msgid "Coordination with upstream developers"
msgstr "開発元/上流 (upstream) の開発者との調整"

#: ../developer-duties.rst:88
msgid ""
"A big part of your job as Debian maintainer will be to stay in contact "
"with the upstream developers. Debian users will sometimes report bugs "
"that are not specific to Debian to our bug tracking system. These bug "
"reports should be forwarded to the upstream developers so that they can "
"be fixed in a future upstream release. Usually it is best if you can do "
"this, but alternatively, you may ask the bug submitter to do it."
msgstr ""

#: ../developer-duties.rst:95
msgid ""
"While it's not your job to fix non-Debian specific bugs, you may freely "
"do so if you're able. When you make such fixes, be sure to pass them on "
"to the upstream maintainers as well. Debian users and developers will "
"sometimes submit patches to fix upstream bugs — you should evaluate and "
"forward these patches upstream."
msgstr ""
"Debian "
"固有ではないバグの修正はあなたの義務ではないとはいえ、できるなら遠慮なく修正してください。そのような修正を行った際は、上流の開発者にも送ってください。時折"
" Debian "
"ユーザ／開発者が上流のバグを修正するパッチを送ってくる事があります。その場合は、あなたはパッチを確認して上流へ転送する必要があります。"

#: ../developer-duties.rst:101
msgid ""
"In cases where a bug report is forwarded upstream, it may be helpful to "
"remember that the bts-link service can help with synchronizing states "
"between the upstream bug tracker and the Debian one."
msgstr ""

#: ../developer-duties.rst:105
msgid ""
"If you need to modify the upstream sources in order to build a policy "
"compliant package, then you should propose a nice fix to the upstream "
"developers which can be included there, so that you won't have to modify "
"the sources of the next upstream version. Whatever changes you need, "
"always try not to fork from the upstream sources."
msgstr "ポリシーに準拠したパッケージをビルドするために上流のソースに手を入れる必要がある場合、以降の上流でのリリースにおいて手を入れなくても済むために、ここで含まれる修正を上流の開発者にとって良い形で提案する必要があります。必要な変更が何であれ、上流のソースからフォークしないように常に試みてください。"

#: ../developer-duties.rst:111
msgid ""
"If you find that the upstream developers are or become hostile towards "
"Debian or the free software community, you may want to re-consider the "
"need to include the software in Debian. Sometimes the social cost to the "
"Debian community is not worth the benefits the software may bring."
msgstr ""
"開発元の開発者らが Debian "
"やフリーソフトウェアコミュニティに対して敵対的である、あるいは敵対的になってきているのを見つけた場合は、ソフトウェアを Debian "
"に含める必要があるかを再考しなければならなくなるでしょう。時折 Debian "
"コミュニティに対する社会的なコストは、そのソフトウェアがもたらすであろう利益に見合わない場合があります。"

#: ../developer-duties.rst:117
msgid "Administrative Duties"
msgstr "管理者的な責務"

#: ../developer-duties.rst:119
msgid ""
"A project of the size of Debian relies on some administrative "
"infrastructure to keep track of everything. As a project member, you have"
" some duties to ensure everything keeps running smoothly."
msgstr ""
"Debian "
"のような大きさのプロジェクトは、あらゆる事を追いかけられる管理者用のインフラに依っています。プロジェクトメンバーとして、あらゆる物事が滞り無く進むように、あなたにはいくつかの義務があります。"

#: ../developer-duties.rst:126
msgid "Maintaining your Debian information"
msgstr "あなたの Debian に関する情報をメンテナンスする"

#: ../developer-duties.rst:128
msgid ""
"There's a LDAP database containing information about Debian developers at"
" https://db.debian.org/\\ . You should enter your information there and "
"update it as it changes. Most notably, make sure that the address where "
"your debian.org email gets forwarded to is always up to date, as well as "
"the address where you get your debian-private subscription if you choose "
"to subscribe there."
msgstr ""
"Debian 開発者に関する情報が含まれた LDAP データベースが https://db.debian.org/\\ "
"にあります。ここに情報を入力して、情報に変更があった際に更新する必要があります。特に、あなたの debian.org "
"アドレス宛メールの転送先アドレスが常に最新になっているのを必ず確認してください。debian-private "
"の購読をここで設定した場合、そのメールを受け取るアドレスについても同様です。"

#: ../developer-duties.rst:135
msgid "For more information about the database, please see :ref:`devel-db`."
msgstr "データベースについての詳細は :ref:`devel-db` を参照してください。"

#: ../developer-duties.rst:140
msgid "Maintaining your public key"
msgstr "公開鍵をメンテナンスする"

#: ../developer-duties.rst:142
msgid ""
"Be very careful with your private keys. Do not place them on any public "
"servers or multiuser machines, such as the Debian servers (see :ref"
":`server-machines`). Back your keys up; keep a copy offline. Read the "
"documentation that comes with your software; read the `PGP FAQ "
"<http://www.cam.ac.uk.pgp.net/pgpnet/pgp-faq/>`__ and `OpenPGP Best "
"Practices <https://riseup.net/en/security/message-security/openpgp/best-"
"practices>`__."
msgstr ""

#: ../developer-duties.rst:149
msgid ""
"You need to ensure not only that your key is secure against being stolen,"
" but also that it is secure against being lost. Generate and make a copy "
"(best also in paper form) of your revocation certificate; this is needed "
"if your key is lost."
msgstr ""
"鍵が盗難に対してだけではなく、紛失についても安全であることを保証する必要があります。失効証明書 (revocation certificate) "
"を生成してコピーを作って下さい (紙にも出力しておくのがベストです)。これは鍵を紛失した場合に必要になります。"

#: ../developer-duties.rst:154
msgid ""
"If you add signatures to your public key, or add user identities, you can"
" update the Debian key ring by sending your key to the key server at "
"``keyring.debian.org``. Updates are processed at least once a month by "
"the ``debian-keyring`` package maintainers."
msgstr ""
"公開鍵に対して、署名したり身元情報を追加したりなどしたら、鍵を ``keyring.debian.org`` の鍵サーバに送付することで "
"Debian キーリングを更新できます。更新は少なくとも月に1度は ``debian-keyring`` パッケージメンテナによって実施されます。"

#: ../developer-duties.rst:159
msgid ""
"If you need to add a completely new key or remove an old key, you need to"
" get the new key signed by another developer. If the old key is "
"compromised or invalid, you also have to add the revocation certificate. "
"If there is no real reason for a new key, the Keyring Maintainers might "
"reject the new key. Details can be found at "
"https://keyring.debian.org/replacing_keys.html\\ ."
msgstr "まったく新しい鍵を追加したりあるいは古い鍵を削除したりする必要がある時は、別の開発者に署名された新しい鍵が必要になります。以前の鍵が侵害されたり利用不可能になった場合には、失効証明書 (revocation certificate) も追加する必要があります。新しい鍵が本当に必要な理由が見当たらない場合は、Keyring メンテナは新しい鍵を拒否することがあります。詳細は https://keyring.debian.org/replacing_keys.html\\  で確認できます。"

#: ../developer-duties.rst:166
msgid "The same key extraction routines discussed in :ref:`registering` apply."
msgstr "同様に鍵の取り出し方について :ref:`registering` で説明されています。"

#: ../developer-duties.rst:169
msgid ""
"You can find a more in-depth discussion of Debian key maintenance in the "
"documentation of the ``debian-keyring`` package and the "
"https://keyring.debian.org/ site."
msgstr "Debian での鍵のメンテナンスについて、より突っ込んだ議論を ``debian-keyring`` パッケージ中のドキュメントおよび\\ https://keyring.debian.org/ サイトで知ることができます。"

#: ../developer-duties.rst:174
msgid "Voting"
msgstr "投票をする"

#: ../developer-duties.rst:176
msgid ""
"Even though Debian isn't really a democracy, we use a democratic process "
"to elect our leaders and to approve general resolutions. These procedures"
" are defined by the `Debian Constitution "
"<https://www.debian.org/devel/constitution>`__."
msgstr ""
"Debian "
"は本来の意味での民主主義ではありませんが、我々はリーダーの選出や一般投票の承認において民主主義的なプロセスを利用しています。これらの手続きについては、\\"
" `Debian 憲章 <https://www.debian.org/devel/constitution>`__\\ で規程されています。"

#: ../developer-duties.rst:181
msgid ""
"Other than the yearly leader election, votes are not routinely held, and "
"they are not undertaken lightly. Each proposal is first discussed on the "
"``debian-vote@lists.debian.org`` mailing list and it requires several "
"endorsements before the project secretary starts the voting procedure."
msgstr ""
"毎年のリーダー選挙以外には、投票は定期的には実施されず、軽々しく提案されるものではありません。提案はそれぞれ ``debian-"
"vote@lists.debian.org`` "
"メーリングリストでまず議論され、プロジェクトの書記担当者が投票手順を開始する前に複数のエンドースメントを必要とします。"

#: ../developer-duties.rst:186
msgid ""
"You don't have to track the pre-vote discussions, as the secretary will "
"issue several calls for votes on ``debian-devel-"
"announce@lists.debian.org`` (and all developers are expected to be "
"subscribed to that list). Democracy doesn't work well if people don't "
"take part in the vote, which is why we encourage all developers to vote. "
"Voting is conducted via GPG-signed/encrypted email messages."
msgstr ""
"書記担当者が ``debian-devel-announce@lists.debian.org`` "
"上で複数回投票の呼びかけを行うので、投票前の議論を追いかける必要はありません "
"(全開発者がこのメーリングリストを購読することが求められています)。民主主義は、人々が投票に参加しないと正常に機能しません。これが我々が全ての開発者に投票を勧める理由です。投票は"
" GPG によって署名／暗号化されたメールによって行われます。"

#: ../developer-duties.rst:194
msgid ""
"The list of all proposals (past and current) is available on the `Debian "
"Voting Information <https://www.debian.org/vote/>`__ page, along with "
"information on how to make, second and vote on proposals."
msgstr ""
"(過去と現在の) 全ての提案リストが `Debian 投票情報 <https://www.debian.org/vote/>`__\\ "
"ページで閲覧できます。提案について、どの様に起案され、支持され、投票が行われたのかという関連情報の確認が可能になっています。"

#: ../developer-duties.rst:201
msgid "Going on vacation gracefully"
msgstr "優雅に休暇を取る"

#: ../developer-duties.rst:203
msgid ""
"It is common for developers to have periods of absence, whether those are"
" planned vacations or simply being buried in other work. The important "
"thing to notice is that other developers need to know that you're on "
"vacation so that they can do whatever is needed if a problem occurs with "
"your packages or other duties in the project."
msgstr "予定していた休暇にせよ、それとも単に他の作業で忙しいにせよ、開発者が不在になることがあるのはごく普通のことです。注意すべき重要な点は、他の開発者達があなたが休暇中であるのを知る必要があることと、あなたのパッケージについて問題が起こった場合やプロジェクト内での責務を果たすのに問題が生じたという様な場合は、必要なことを彼らが何であってもできるようにすることです。"

#: ../developer-duties.rst:209
msgid ""
"Usually this means that other developers are allowed to NMU (see "
":ref:`nmu`) your package if a big problem (release critical bug, security"
" update, etc.) occurs while you're on vacation. Sometimes it's nothing as"
" critical as that, but it's still appropriate to let others know that "
"you're unavailable."
msgstr ""
"通常、これはあなたが休暇中にあなたのパッケージが大きな問題 (リリースクリティカルバグやセキュリティ更新など) "
"となっている場合に、他の開発者に対して NMU (:ref:`nmu` 参照) "
"を許可することを意味しています。大抵の場合はそれほど致命的なことはおきませんが、他の開発者に対してあなたが作業できない状態であることを知らせるのは重要です。"

#: ../developer-duties.rst:215
msgid ""
"In order to inform the other developers, there are two things that you "
"should do. First send a mail to ``debian-private@lists.debian.org`` with "
"[VAC] prepended to the subject of your message [1]_ and state the period "
"of time when you will be on vacation. You can also give some special "
"instructions on what to do if a problem occurs."
msgstr ""
"他の開発者に通知するために行わなければならないことが 2 つあります。まず、\\ ``debian-"
"private@lists.debian.org`` にサブジェクトの先頭に [VAC] と付けたメールを送り "
"[1]_、いつまで休暇なのかを示しておきます。何か問題が起きた際への特別な指示を書いておくこともできます。"

#: ../developer-duties.rst:221
msgid ""
"The other thing to do is to mark yourself as on vacation in the :ref"
":`devel-db` (this information is only accessible to Debian developers). "
"Don't forget to remove the on vacation flag when you come back!"
msgstr ""
"他に行うべき事は :ref:`devel-db` 上 であなたを vacation とマークする事です (この情報は Debian "
"開発者のみがアクセスできます)。休暇から戻った時には vacation フラグを削除するのを忘れないように!"

#: ../developer-duties.rst:225
msgid ""
"Ideally, you should sign up at the `GPG coordination pages "
"<https://wiki.debian.org/Keysigning>`__ when booking a holiday and check "
"if anyone there is looking for signing. This is especially important when"
" people go to exotic places where we don't have any developers yet but "
"where there are people who are interested in applying."
msgstr ""
"理想的には、休暇にあわせて `GPG coordination pages "
"<https://wiki.debian.org/Keysigning>`__ "
"に登録して、誰かサインを希望している人がいるかどうかをチェックします。開発者がまだ誰もいないけれども応募に興味を持っている人がいるようなエキゾチックな場所に行く場合、これは特に重要です。"

#: ../developer-duties.rst:235
msgid "Retiring"
msgstr "脱退について"

#: ../developer-duties.rst:237
msgid ""
"If you choose to leave the Debian project, you should make sure you do "
"the following steps:"
msgstr "Debian プロジェクトから去るのを決めた場合は、以下の手順に従ってください:"

#: ../developer-duties.rst:240
msgid "Orphan all your packages, as described in :ref:`orphaning`."
msgstr ":ref:`orphaning` の記述に従って、全てのパッケージを放棄 (orphan) してください。"

#: ../developer-duties.rst:242
msgid "Remove yourself from uploaders for co- or team-maintained packages."
msgstr "一緒にメンテナンスしているパッケージやチームとしてメンテナンスしているパッケージのUploaders:フィールドから自身を削除してください。"

#: ../developer-duties.rst:244
msgid ""
"If you received mails via a @debian.org e-mail alias (e.g. "
"press@debian.org) and would like to get removed, open a RT ticket for the"
" Debian System Administrators. Just send an e-mail to "
"``admin@rt.debian.org`` with \"Debian RT\" somewhere in the subject "
"stating from which aliases you'd like to get removed."
msgstr ""
"@debian.org メールアドレスの alias (例: press@debian.org) "
"経由でメールを受け取っていて削除したい場合、Debian システム管理者に対する RT "
"チケットをオープンしてください。チケットをオープンするには、削除したい alias のアドレスから、\\ "
"``admin@rt.debian.org`` 宛でサブジェクトのどこかに \"Debian RT\" と入れて送信します。"

#: ../developer-duties.rst:250
msgid ""
"Please remember to also retire from teams, e.g. remove yourself from team"
" wiki pages or salsa groups."
msgstr ""

#: ../developer-duties.rst:253
msgid ""
"Use the link https://nm.debian.org/process/emeritus to log in to "
"nm.debian.org, request emeritus status and write a goodbye message that "
"will be automatically posted on debian-private."
msgstr ""

#: ../developer-duties.rst:257
msgid ""
"Authentification to the NM site requires an SSO browser certificate. You "
"can generate them on https://sso.debian.org."
msgstr ""

#: ../developer-duties.rst:260
msgid ""
"In the case you run into problems opening the retirement process "
"yourself, contact NM front desk using ``nm@debian.org``"
msgstr ""

#: ../developer-duties.rst:263
msgid ""
"It is important that the above process is followed, because finding "
"inactive developers and orphaning their packages takes significant time "
"and effort."
msgstr "上記のプロセスに従うのは重要です。何故なら活動を停止している開発者を探してパッケージを放棄するのは、非常に時間と手間がかかることだからです。"

#: ../developer-duties.rst:270
msgid "Returning after retirement"
msgstr "リタイア後に再加入する"

#: ../developer-duties.rst:272
msgid ""
"A retired developer's account is marked as \"emeritus\" when the process "
"in :ref:`s3.7` is followed, and \"removed\" otherwise. Retired developers"
" with an \"emeritus\" account can get their account re-activated as "
"follows:"
msgstr "リタイアした開発者のアカウントは、\\ :ref:`s3.7` の手続きが開始された際に「emeritus」であるとマークされ、それ以外の場合は「removed」となります。「emeritus」アカウントになっているリタイアした開発者は、以下のようにすればアカウントを再度有効にできます:"

#: ../developer-duties.rst:277
msgid ""
"Get access to an alioth account (either by remembering the credentials "
"for your old guest account or by requesting a new one as decribed at `SSO"
" Debian wiki page "
"<https://wiki.debian.org/DebianSingleSignOn#If_you_ARE_NOT_.28yet.29_a_Debian_Developer>`__."
msgstr ""

#: ../developer-duties.rst:282
msgid "Mail ``nm@debian.org`` for further instructions."
msgstr ""

#: ../developer-duties.rst:284
msgid ""
"Go through a shortened NM process (to ensure that the returning developer"
" still knows important parts of P&P and T&S)."
msgstr "短縮された NM プロセスを通過します (リタイアした開発者が P&P および T&S の肝心な部分を覚えているのを確認するためです)。"

#: ../developer-duties.rst:287
msgid ""
"Retired developers with a \"removed\" account need to go through full NM "
"again."
msgstr "リタイアした開発者で「removed」アカウントの人は、NM をもう一度通り抜ける必要があります。"

#: ../developer-duties.rst:291
msgid ""
"This is so that the message can be easily filtered by people who don't "
"want to read vacation notices."
msgstr "これは、休暇のメッセージを読みたくない人がメッセージを簡単に振り分け可能にするためです。"

#~ msgid ""
#~ "Send an gpg-signed email announcing "
#~ "your retirement to ``debian-"
#~ "private@lists.debian.org``."
#~ msgstr "GPG でサインされたメールを ``debian-private@lists.debian.org`` に投げてください。"

#~ msgid ""
#~ "Notify the Debian key ring maintainers"
#~ " that you are leaving by opening "
#~ "a ticket in Debian RT by sending"
#~ " a mail to ``keyring@rt.debian.org`` with"
#~ " the words \"Debian RT\" somewhere in"
#~ " the subject line (case doesn't "
#~ "matter)."
#~ msgstr ""

#~ msgid "Contact ``da-manager@debian.org``."
#~ msgstr "``da-manager@debian.org`` に連絡を取ります"

#~ msgid ""
#~ "Prove that they still control the "
#~ "GPG key associated with the account, "
#~ "or provide proof of identify on a"
#~ " new GPG key, with at least two"
#~ " signatures from other developers."
#~ msgstr ""
#~ "アカウントに紐付けられた GPG 鍵を今でも管理していることを証明する、あるいは新しい GPG "
#~ "鍵について、他の開発者から少なくとも 2 つの署名を受けることにより身分証明を行う。"

